const readPackageJson = require('../index')

describe('Read package.json file', () => {
  test('valid file', async () => {
    const pkg = await readPackageJson('./test/vpkg.json')
    expect(pkg.version).toBe('1.0.0')
  })
    test('not valid file', async () => {
        const filePath = './test/nvpkg.json'
        return readPackageJson(filePath).catch(e =>
            expect(e).toEqual(new Error(`Failed to read package file with param: ${filePath}`)),
        )
    })
})
