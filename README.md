### ab-read-package
Read package.json file

#### How to install
```
    npm i ab-read-package
```

### How to use

```javascript
    const readPackage = require('ab-read-package')
    readPackage(filePath)
        .then(pacakge => ...)
```

#### API
- readPackage(filePath)
    - filePath: directory | full path to package.json file
    - returns promise that resolve to (json) object