'use strict'
const fs = require('fs')
const {promisify} = require('util')

const stats = promisify(fs.stat)
module.exports = async (path) => {
  try {
    const stat = await stats(path)
    return stat.isDirectory()
  } catch (e) {
    return false
  }
}
