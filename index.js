#!/usr/bin/env node
'use strict'

const fs = require('fs')
const path = require('path')
const {promisify} = require('util')
const normalizeData = require('normalize-package-data')
const isDir = require('./lib/is-directory')

const readFile = promisify(fs.readFile)

const readPackageFile = async p => {
  try {
    let filePath = p
    const isDirectory = await isDir(p)
    if (isDirectory) {
      filePath = path.join(p, 'package.json')
    }
    console.log(process.cwd)
    const packageData = await readFile(filePath, 'utf8')
    try {
      const packageJson = JSON.parse(packageData)
      normalizeData(packageJson)
      return packageJson
    } catch (e) {
      throw new Error('Failed to parse package.json file: ', e)
    }
  } catch (e) {
    throw new Error(`Failed to read package file with param: ${p}`)
  }
}

module.exports = readPackageFile
